from django.contrib.messages.views import SuccessMessageMixin
from django.db.models import Q
from django.urls import reverse_lazy
from django.views.generic import UpdateView, CreateView, ListView
from authors.forms import AuthorCreateForm
from authors.models import Authors


class AuthorView(SuccessMessageMixin, CreateView):
    form_class = AuthorCreateForm
    template_name = 'authors/author.html'
    success_url = reverse_lazy('author_list')
    success_message = "%(author_name)s was created successfully"

    def get_form_kwargs(self):
        osman = super().get_form_kwargs()
        osman["request"] = self.request
        return osman


class AuthorListView(ListView):
    model = Authors
    template_name = 'authors/author_list.html'
    context_object_name = 'authors'
    # login_url = '/login'
    # ordering = ['-upload_date']


class AuthorUpdateView(UpdateView):
    model = Authors
    form_class = AuthorCreateForm
    template_name = 'authors/author.html'



