from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import get_user_model
from django import forms
from authors.models import Authors


class AuthorCreateForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request")
        super().__init__(*args, **kwargs)

    def save(self, commit=True):
        obj = super(AuthorCreateForm, self).save(False)
        obj.owner = self.request.user
        obj.save(commit)
        return obj

    class Meta:
        model = Authors
        fields = [
            'author_name', 'biography'
        ]


