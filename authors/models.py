from django.db import models
from django.conf import settings



class Authors(models.Model):
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True, blank=True)
    # documents = models.ForeignKey(Documents, on_delete=models.CASCADE, related_name='documents')
    author_name = models.CharField(max_length=50)
    biography = models.TextField(max_length=500)

    def __str__(self):
        return self.author_name
