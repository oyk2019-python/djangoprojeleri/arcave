from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import get_user_model
from django import forms
from authors.models import Authors
from .models import UserTable


class RealDateInput(forms.DateInput):
    input_type = "date"


class UserRegisterForm(UserCreationForm):
    birthday = forms.DateField(widget=RealDateInput, input_formats=['%Y-%m-%d'])

    class Meta(UserCreationForm.Meta):
        model = get_user_model()
        fields = UserCreationForm.Meta.fields + (
            'username', 'email', 'location', 'birthday', 'image')


class UserLoginForm(AuthenticationForm):
    pass


class ProfileUpdateForm(forms.ModelForm):
    birthday = forms.DateField(widget=RealDateInput, input_formats=['%Y-%m-%d'])

    class Meta(UserTable.Meta):
        model = UserTable
        fields = [
            'username', 'email', 'location', 'birthday', 'image'

        ]
