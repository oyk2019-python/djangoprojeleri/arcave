from django.db import models
from django.contrib.auth.models import AbstractUser
from django.urls import reverse


class UserTable(AbstractUser):

    location = models.CharField(null=True, max_length=20, blank=True, verbose_name=("Location"))
    birthday = models.DateField(null=True, blank=True, verbose_name=("Birthday"))
    image = models.ImageField(null=True, blank=True, upload_to="usertable_image", verbose_name=("Image"))

    def get_absolute_url(self):
        return reverse('user_detail', kwargs={'pk': self.pk})