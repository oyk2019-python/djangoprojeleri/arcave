from django.contrib import admin
from .models import UserTable
from django.contrib.auth.admin import UserAdmin

@admin.register(UserTable)
class UserTable(UserAdmin):
    fieldsets = UserAdmin.fieldsets + (
                ('Custom Fields', {'fields': ('image', )}),
    )
