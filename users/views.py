from django.contrib.auth import authenticate, login
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import FormView, UpdateView, DetailView
from django.urls import reverse_lazy
from users.forms import UserRegisterForm, ProfileUpdateForm
from users.models import UserTable


class RegistrationView(FormView):
    form_class = UserRegisterForm
    model = UserTable
    # template_name = 'users/register.html'
    success_url = reverse_lazy("me")

    def form_valid(self, form):
        resp = super().form_valid(form)
        user = authenticate(username=form.cleaned_data["username"], password=form.cleaned_data["password1"])
        login(self.request, user)
        return resp

    def get_context_data(self, **kwargs):
        kwargs = super().get_context_data(**kwargs)
        kwargs["is_update"] = False
        return kwargs


class LoginView(FormView):
    form_class = AuthenticationForm
    template_name = 'users/login.html'
    success_url = reverse_lazy('documentlist')


class ProfileUpdateView(LoginRequiredMixin, UpdateView):
    model = UserTable
    form_class = ProfileUpdateForm

    def get_success_url(self):
        return reverse_lazy("me")

    def get_object(self, queryset=None):
        return self.request.user

    def get_context_data(self, **kwargs):
        kwargs = super().get_context_data(**kwargs)
        kwargs["is_update"] = True
        return kwargs


class UserDetailView(LoginRequiredMixin, DetailView):
    model = UserTable
    context_object_name = 'user_detail'
    is_me = False

    def get_object(self, queryset=None):
        if self.is_me:
            return self.request.user
        else:
            return super().get_object(queryset)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["is_me"] = self.is_me
        return context


