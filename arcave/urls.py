"""arcave URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.contrib import admin
from django.views.generic import TemplateView
from django.contrib.auth.views import LogoutView, LoginView
from django.urls import path, re_path
from django.views.static import serve

from documents.views import DocumentView, DocumentCommentView, DocumentUpdateView, DocumentActionView, \
    DocumentListView, DocumentCommentListView, DocumentsSearchView, DocumentDetailView, DokumanTag, AuthorSearch
from users.views import RegistrationView, ProfileUpdateView, UserDetailView
from authors.views import AuthorView, AuthorUpdateView, AuthorListView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', TemplateView.as_view(template_name="users/homepage.html"), name='homepage'),
    path('users/register/', RegistrationView.as_view(), name='register'),
    path('login/', LoginView.as_view(), name='login'),
    path('authors/author/', AuthorView.as_view(), name='author_adds'),
    path('authors/author_list/', AuthorListView.as_view(), name='author_list'),
    path('documents/<int:document_id>/documentcomment/', DocumentCommentView.as_view(), name='documentcomment'),
    path('documents/document/', DocumentView.as_view(), name='document'),
    path('detail/<int:document_id>/edit_form/', DocumentUpdateView.as_view(), name='document_edit_form'),
    path('detail/<int:document_id>/edit/', DocumentActionView.as_view(action="edit"), name='doc_edit'),
    path('authors/update/', AuthorUpdateView.as_view(), name='author_update'),
    path('documents/<int:document_id>/', DocumentDetailView.as_view(), name='detail'),
    path('detail/<int:document_id>/remove/', DocumentActionView.as_view(action="remove", validate=True),
         name='doc_remove'),
    path('logout', LogoutView.as_view(), name='logout'),
    path('documents/', DocumentListView.as_view(), name='document_list'),
    path('documents/document_comment_list/', DocumentCommentListView.as_view(), name='document_comment_list'),
    path('search_result/', DocumentsSearchView.as_view(), name='search'),
    path('category/<str:category>/', DokumanTag.as_view(), name='tag_result'),
    path('author_name/<str:author_name>/', AuthorSearch.as_view(), name='author_result'),

    path('profile/', ProfileUpdateView.as_view(), name='update_profile'),
    path('detail/me/', UserDetailView.as_view(is_me=True), name='me'),
    path('detail/<int:pk>/', UserDetailView.as_view(), name='user_detail'),

]
if settings.DEBUG:
    urlpatterns.append(re_path(r'^media/(?P<path>.*)$',
                               serve,
                               {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}))
