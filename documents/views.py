from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.db.models import Q
from django.shortcuts import render
from django.urls import reverse, reverse_lazy
from django.utils.translation import gettext_noop
from documents.forms import DocumentCreateForm, DocumentCommentCreateForm, DocumentUpdateForm
from django.views.generic import UpdateView, CreateView, DetailView, ListView, RedirectView
from documents.models import Documents, DocumentsComment


class DocumentListView(LoginRequiredMixin, ListView):
    model = Documents
    template_name = 'documents/documentlist.html'
    context_object_name = 'documents'
    login_url = '/login'
    ordering = ['-upload_date']


class DocumentCommentListView(LoginRequiredMixin, ListView):
    model = DocumentsComment
    context_object_name = 'documentcomment'
    template_name = 'documents/document_comment_list.html'
    success_url = reverse_lazy('document_comment_list')


class DocumentView(SuccessMessageMixin, LoginRequiredMixin, CreateView):
    form_class = DocumentCreateForm
    template_name = 'documents/document.html'
    success_url = reverse_lazy('document_list')
    success_message = " Document created successfully"


class DocumentCommentView(SuccessMessageMixin, LoginRequiredMixin, CreateView):
    form_class = DocumentCommentCreateForm
    template_name = 'documents/documentcomment.html'
    success_url = reverse_lazy('document_comment_list')
    model = DocumentsComment
    success_message = " Comment created successfully"

    def get_object(self):
        document = Documents.objects.get(id=self.kwargs['document_id'])
        return document

    def form_valid(self, form):
        form.instance.documents = self.get_object()
        form.instance.owner = self.request.user
        return super(DocumentCommentView, self).form_valid(form)


class DocumentUpdateView(SuccessMessageMixin, LoginRequiredMixin, UpdateView):
    model = Documents
    form_class = DocumentUpdateForm
    success_url = reverse_lazy("documentlist")
    pk_url_kwarg = "document_id"
    success_message = "Updated successfully"

    def get_success_url(self):
        return reverse("detail", kwargs={"document_id": self.kwargs["document_id"]})

    # def form_valid(self, form):
    #     form.save()
    #     # Formlarımızda sorun yoksa bu metod çalışacak.
    #     # Bildirimimizi messages.success ile ekleyeceğiz.
    #     messages.success(self.request, 'It has been updated.')
    #     return super().form_valid(form)
    #
    #
    # def form_invalid(self, form):
    #     # Formlarımızda sorun varsa bu metod çalışacak.
    #     # Bir hata mesajı göstermek istediğimiz için messages.error kullanıyoruz.
    #     messages.error(self.request, 'It could not be updated, please check the form.')
    #     return super().form_invalid(form)
    #


class DocumentDetailView(DetailView):
    model = Documents
    pk_url_kwarg = "document_id"


class DocumentActionView(LoginRequiredMixin, RedirectView):
    action = None
    validate = False
    url = None

    def get_redirect_url(self, *args, **kwargs):
        return self.url or reverse("detail", kwargs={'document_id': self.kwargs["document_id"]})

    def apply_action(self, action):
        if action == gettext_noop("remove"):
            self.document.delete()
            self.url = reverse("documentlist")
        elif action == gettext_noop("edit"):
            self.url = reverse("document_edit_form", kwargs={'document_id': self.kwargs["document_id"]})
        else:
            pass

    def get(self, request, *args, **kwargs):
        self.document = Documents.objects.get(id=self.kwargs["document_id"])
        if request.GET.get("valid") == 'false':
            return super().get(request, *args, **kwargs)
        elif self.validate and not request.GET.get("valid") == 'true':
            return render(request, "users/validate.html", {"action": self.action, "object": self.document})
        else:
            self.apply_action(self.action)
            return super().get(request, *args, **kwargs)


class DocumentsSearchView(ListView):
    model = Documents
    template_name = 'documents/search_result.html'

    def get_queryset(self):
        query = self.request.GET.get('q')
        search_result = Documents.objects.filter(
            Q(author__documents__name__icontains=query)
        )
        return search_result


def num_post(request):
    return Documents.objects.count()
    # return render(request, 'documentlist.html', {'num_post': num_post})


class DokumanTag(ListView):
    model = Documents
    template_name = 'documents/tag_result.html'
    context_object_name = 'qs'

    def get_queryset(self):
        return self.model.objects.filter(Q(category__icontains=self.kwargs['category']))


class AuthorSearch(ListView):
    model = Documents
    template_name = 'documents/author_result.html'
    context_object_name = 'j'

    def get_queryset(self):
        return self.model.objects.filter(Q(author__author_name=self.kwargs))
