import hashlib

from django.db import models
from django.conf import settings
from django.forms import Media

from authors.models import Authors
from django.core.files.storage import FileSystemStorage
# Create your models here.

kategori_secim = ('Scene', 'Scene'), ('Art', 'Art'), ('Political', 'Political'), ('Scholar', 'Scholar'), (
    'Child', 'Child'), ('Philosophy', 'Philosophy'), ('Religion', 'Religion'), ('History', 'History')


class MediaFileSystemStorage(FileSystemStorage):
    def get_available_name(self, name, max_length=None):
        if max_length and len(name) > max_length:
            raise(Exception("name's length is greater than max_length"))
        return name

    def _save(self, name, content):
        if self.exists(name):
            return name
        return super(MediaFileSystemStorage, self)._save(name, content)


class Documents(models.Model):
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.DO_NOTHING)
    name = models.CharField(max_length=50, verbose_name='Document Name:')
    category = models.CharField(choices=kategori_secim, max_length=50, verbose_name='Category:')
    file = models.FileField(verbose_name='File to upload', storage=MediaFileSystemStorage(), null=False, blank=True)
    date = models.DateTimeField()
    upload_date = models.DateTimeField(auto_now=True, blank=True)
    # size = models.IntegerField()
    description = models.TextField(max_length=400, blank=True)
    author = models.ForeignKey(Authors, null=True, on_delete=models.DO_NOTHING)
    md5sum = models.CharField(max_length=36)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.pk:  # file is new
            md5 = hashlib.md5()
            for chunk in self.file.chunks():
                md5.update(chunk)
            self.md5sum = md5.hexdigest()
        super(Documents, self).save(*args, **kwargs)


class DocumentsComment(models.Model):
    documents = models.ForeignKey(Documents, on_delete=models.CASCADE, related_name='comments', verbose_name='Document Name:')
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.DO_NOTHING)
    comment = models.TextField(max_length=300)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.comment
