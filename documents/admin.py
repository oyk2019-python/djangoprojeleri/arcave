from django.contrib import admin
from .models import Documents, DocumentsComment
# Register your models here.


admin.site.register(Documents)
admin.site.register(DocumentsComment)
