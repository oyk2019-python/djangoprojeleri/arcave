from django import forms
from documents.models import Documents, DocumentsComment


class RealDateInput(forms.DateInput):
    input_type = 'date'


class DocumentCreateForm(forms.ModelForm):
    date = forms.DateField(widget=RealDateInput, input_formats=['%Y-%m-%d'])

    class Meta:
        model = Documents

        fields = [
            'owner', 'name', 'category', 'date', 'file', 'description', 'author'
        ]
        widget = {
            'date': RealDateInput
        }


class DocumentUpdateForm(forms.ModelForm):
    date = forms.DateField(widget=RealDateInput, input_formats=['%Y-%m-%d'])

    class Meta:
        model = Documents
        fields = ['owner', 'name', 'category', 'date', 'file', 'description', 'author']


class DocumentCommentCreateForm(forms.ModelForm):
    # def __init__(self, *args, **kwargs):
    #     # self.request = kwargs.pop("request")
    #     self.request = kwargs.pop('request')
    #     super(DocumentCommentCreateForm, self).__init__(*args, **kwargs)
    #
    # def save(self, commit=True):
    #     obj = super(DocumentCommentCreateForm, self).save(False)
    #     obj.owner = self.request.user
    #     obj.save()
    #     return obj

    class Meta:
        model = DocumentsComment

        fields = [
         'comment',
        ]


class CommentForm(forms.ModelForm):
    class Meta:
        model = DocumentsComment
        fields = [
            'comment',
        ]
